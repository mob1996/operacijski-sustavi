package lamport_dretve;

import java.util.ArrayList;
import java.util.Random;

public class lamport_dretve {
	
	public static int zadnji=0;
	public static int brojStolova;
	public static int brojDretvi;
	public static int[] trazim;
	public static int[] broj;
	public static int[] stolovi;
	public static int brojZauzetih=0;
	
	public static class TableThread extends Thread {
		int TID;
		int tablica;
		
		public TableThread(int TID) {
			this.TID=TID;
			this.tablica=TID-1;
		}
		
		public void run(){
			Random rand = new Random();
			
			while(true){
				ulaz_KO();
				if(brojStolova<=brojZauzetih){
					izlaz_KO();
					break;
				}
				izlaz_KO();
				int nasumicniStol= rand.nextInt(brojStolova);
				System.out.println("Dretva " + TID + ": odabirem broj " + (nasumicniStol+1));
				
				ulaz_KO();
				if(stolovi[nasumicniStol]!=0){
					System.out.println("Dretva " + TID + " neuspjela rezervacija stola " + (nasumicniStol+1) + ", stanje:\n" + stanje());
				} else {
					stolovi[nasumicniStol] = TID;
					System.out.println("Dretva " + TID + ": rezerviram  stol " + (nasumicniStol+1) + ", stanje:\n" + stanje());
					brojZauzetih++;
				}
				
				izlaz_KO();
				
				
				int sleepTime= rand.nextInt(2500)+2500;
				try {
					sleep(sleepTime);
				} catch (InterruptedException e) {
				}
				
			}
			
		}
		
		public void ulaz_KO(){
			trazim[tablica] = 1;
			broj[tablica] = zadnji;
			zadnji++;
			trazim[tablica] = 0;
			
			for(int j=0; j<brojDretvi;j++){
				while(trazim[j]==1);
				while(broj[j]!=0 && (broj[j] < broj[tablica] || (broj[j]==broj[tablica] && j < tablica)));
			}
		}
		
		public void izlaz_KO(){
			broj[tablica]=0;
		}
		
		
		public String stanje(){
			String stanje="";
			for(int i=0;i<stolovi.length;i++){
				if(stolovi[i]==0) {
					stanje+="-";
				} else {
					stanje+=stolovi[i];
				}
			}
			return stanje;
		}
		
	}
	
	public static void main(String[] args) throws InterruptedException {
		if(args.length!= 2){
			System.err.println("Invalid numer of arguments!!!");
			return;
		}
		brojDretvi= Integer.valueOf(args[0]);
		brojStolova= Integer.valueOf(args[1]);
		trazim= new int[brojDretvi];
		broj= new int[brojDretvi];
		stolovi= new int[brojStolova];
		ArrayList<TableThread> threads =new ArrayList<>();
		
		for(int i=1; i<=brojDretvi;i++){
			TableThread t= new TableThread(i);
			t.start();
			threads.add(t);
		}
		
		for(TableThread t:threads){
			t.join();
		}
		
		System.out.println("THE END");
	}

}