#include <stdio.h>
#include <stdlib.h>
#include<signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

int *trazim, *broj, *stolovi, *zadnji, *brojZauzetih;
int Id;
int brojStolova, brojProcesa;

void inicijaliziraj(){  
   
   trazim = (int *) shmat(Id, NULL, 0);
   broj = (int *) shmat(Id, NULL, 0) + (brojProcesa+1)*sizeof(int);
   stolovi = (int *) shmat(Id, NULL, 0) + 2*(brojProcesa+1)*sizeof(int);
   zadnji = (int *) shmat(Id, NULL, 0) + 3*(brojProcesa+1)*sizeof(int);
   brojZauzetih = (int *) shmat(Id, NULL, 0) + 3*(brojProcesa+2)*sizeof(int);
   *zadnji = 0;
   *brojZauzetih = 0;
   for(int i=0; i<brojProcesa;i++) { 
      *(trazim+i)=0;
      *(broj+i)=0; 
   }
   for(int i=0; i<brojStolova;i++) { 
      *(stolovi+i) = 0;
   }
}

void brisi(int sig){
   (void) shmdt((char *) trazim);
   (void) shmdt((char *) broj);
   (void) shmdt((char *) stolovi);
   (void) shmctl(Id, IPC_RMID, NULL);
   exit(0);
}

void zadatak(int id) {
   int PID=id+1;
   srand(getpid());
   while(1) {
      ulaz_KO(id);
      if(brojStolova<=*brojZauzetih) {
         izlaz_KO(id);
         break;
      }
      izlaz_KO();
      
    
      sleep(rand()%3);
      int nasumicniStol=rand()%brojStolova;
      printf("Proces %d: odabirem stol %d\n", PID, nasumicniStol+1);
     
      ulaz_KO(id);
      
      int stol = *(stolovi+nasumicniStol);
      if(stol!=0) {
         printf("Proces %d: neuspjela rezervacija stola %d, stanje: \n", PID, nasumicniStol+1);
         stanje();
      } else {
         *(stolovi+nasumicniStol)=PID;
         printf("Proces %d: rezerviram stol %d, stanje: \n", PID, nasumicniStol+1);
         stanje();
         *brojZauzetih+=1;
      }

      izlaz_KO(id);
      
      

   }
}

void ulaz_KO(int id){
   *(trazim+id)=1;
   *(broj+id)=*zadnji;
   *zadnji+=1;
   *(trazim+id)=0;
   
   for(int j=0;j<brojProcesa;j++){
      while(*(trazim+j)==1);
      while(broj[j]!=0 && (broj[j] < broj[id] || (broj[j]==broj[id] && j<id)));
   }
}

void izlaz_KO(int id) {
   *(broj+id)=0;
}

int imaLiSlobodnogMjesta(int id){
   int ima=0;
   ulaz_KO(id);
}

void stanje(){
   for(int i=0;i<brojStolova;i++){
      int stol=*(stolovi+i);
      if(stol==0){
         printf("-");
      } else {
         printf("%d", stol);
      }
   }
   printf("\n");
}


int main (int argc, char *argv[]) {
   
   int i, zauzet;
   if(argc!=3){
      printf("Invalid number of arguments!\n");
   }
   

   brojProcesa= atoi(argv[1]);
   brojStolova= atoi(argv[2]);
   zauzet = brojProcesa*2+brojStolova*100;

   Id = shmget(IPC_PRIVATE, sizeof(int)*zauzet, 0600);   

   if(Id==0) exit(0);

   inicijaliziraj();
   sigset(SIGINT, brisi);

   for(i=0;i<brojProcesa;i++){
      switch(fork()) {
      case 0:
         zadatak(i);
         exit(0);
      case -1:
         printf("Proces se nije stvorio.\n");
         break;
      default:
         break;
      }
   }
   while(i--){
      (void) wait(NULL);
   }
   
   brisi(0);
   return 0;
}




















