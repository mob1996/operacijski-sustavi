import java.util.ArrayList;

public class dretve {

	static long A;
	static int numOfThreads;
	static int timesToAdd;
	
	public static void main(String[] args) throws InterruptedException {
		if(args.length!= 2){
			System.err.println("Invalid numer of arguments!!!");
			return;
		}
		numOfThreads = Integer.valueOf(args[0]);
		timesToAdd = Integer.valueOf(args[1]);
		ArrayList<Thread> threads = new ArrayList<>();	
		for(int i=0; i<numOfThreads;i++) {
			Thread t = new Thread(() -> {
				for(int j=0;j<timesToAdd;j++){
					A++;
				}
			});
			threads.add(t);
			t.start();
		}
		
		for(Thread t:threads){
			t.join();
		}
		
		System.out.println("A = "+A);

	}

}