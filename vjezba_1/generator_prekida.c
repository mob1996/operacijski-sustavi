#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <time.h>

int pid=0;

void prekidna_rutina(int sig){
   /* pošalji SIGKILL procesu 'pid'*/
   kill(pid, SIGKILL);	
   exit(0);
}

int main(int argc, char *argv[]){

   int minSleep=3;
   int maxSleep=5;
   int numOfSignals=4;

   srand((unsigned) time (NULL));
   pid=atoi(argv[1]);
   sigset(SIGINT, prekidna_rutina);

   while(1){
      /* odspavaj 3-5 sekundi */
      /* slučajno odaberi jedan signal (od 4) */
      /* pošalji odabrani signal procesu 'pid' funkcijom kill*/
      int randSleep=rand()%(maxSleep-minSleep+1)+minSleep;
      sleep(randSleep);
      int randSignal = rand()%4;
      switch (randSignal){
	case (0):
           kill(pid, SIGUSR1);
	   break;
	case (1):
	   kill(pid, SIGUSR2);
	   break;
	case (2):
	   kill(pid, SIGTRAP);	
	   break;	
	default:
	   kill(pid, SIGTERM);
	   break;
	}
   }
   return 0;
}
