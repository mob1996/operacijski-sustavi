#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <math.h>
#include <stdlib.h>

int prost (unsigned long n) {
        unsigned long i, max;
        if((n & 1) == 0 )
                return 0;

        max=sqrt(n);
        for(i=3; i<=max;i++){
                if((n%i)==0)
                        return 0;
        }

        return 1;
}

int pauza = 0;
unsigned long broj = 1000000001;
unsigned long zadnji = 1000000001;


void periodicki_ispis() {
        printf("zadnji: %lu\n", zadnji);
}

void postavi_pauzu() {
        pauza = 1-pauza;
}

void prekini(){
        printf("zadnji: %lu\n", zadnji);
        exit(0);
}



int main (void) {
        struct itimerval t;

        sigset(SIGALRM, periodicki_ispis);
        sigset(SIGINT, postavi_pauzu);
        sigset(SIGTERM, prekini);


        t.it_value.tv_sec = 0;
        t.it_value.tv_usec= 500000;

        t.it_interval.tv_sec = 5;
        t.it_interval.tv_usec = 500000;

        setitimer(ITIMER_REAL, &t, NULL);

        while(1){
                if(prost(broj)){
                        zadnji=broj;
                }
                broj++;
                while(pauza)
                        pause();
        }

        return 0;
}







