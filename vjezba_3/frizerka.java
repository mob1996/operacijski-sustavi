
import java.util.Random;
import java.util.concurrent.Semaphore;

public class frizerka {
	public static Random rand= new Random();
	public static boolean otvoreno = false;
	public static Cekaonica cekaonica = new Cekaonica();
	
	public static Semaphore semOtvoreno = new Semaphore(1);
	public static Semaphore semCekaonica = new Semaphore(1,true);
	public static Semaphore semSpavanje = new Semaphore(0);
	public static Semaphore semUzmiKlijenta = new Semaphore(0, true);
	
	static public class Frizerka extends Thread {
		
		public void run(){
			try {
				semOtvoreno.acquire();
				otvoreno=true;
				System.out.println(this+": Otvaram salon");
				semOtvoreno.release();
				
				while(true){
					System.out.println(this+": Spavam dok klijenti ne dodju");
					semSpavanje.acquire();
					
					semCekaonica.acquire();
					if(!cekaonica.isEmpty()){
						semUzmiKlijenta.release();
						semCekaonica.release();
						System.out.println(this+": Idem raditi");
						sleep(2000);
						System.out.println(this+": Klijent gotov");
					} else {
						System.out.println(this+": Zatvaram salon.");
						semCekaonica.release();
						return;
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		
		@Override
		public String toString(){
			return "Frizerka";
		}
	}
	
    static public class Klijent extends Thread {
		private int id;
		
		public Klijent(int id){
			this.id=id;
		}
		
		@Override
		public String toString(){
			return "Klijent("+id+")";
		}
		
		public void run(){
			try {
				semOtvoreno.acquire();
				if(otvoreno==false){
					System.out.println("\t"+this+": Radnja je zatvorena");
					semOtvoreno.release();
					return;
				}
				semOtvoreno.release();
				semCekaonica.acquire();
				if(cekaonica.isFull()){
					System.out.println("\t"+this+": Nema mjesta u cekaoni");
					semCekaonica.release();
					return;
				}
				cekaonica.addKlijenta();
				System.out.println("\t"+this+": Ulazim u cekaonu ("+cekaonica.brZauzetih()+")");
				semCekaonica.release();
				
				System.out.println("\t"+this+ ": Zelim na frizuru");
				semSpavanje.release();
				semUzmiKlijenta.acquire();
				
				semCekaonica.acquire();
				cekaonica.removeKlijenta();
				semCekaonica.release();
				
				
			} catch (InterruptedException e) {
			}
		}
	}
    
    static public class KlijentGenerator extends Thread{
    	int brojKlijenata;
    	
    	public KlijentGenerator(){
    		brojKlijenata= rand.nextInt(5)+5;
    	}
    	
    	@Override
    	public void run(){
    		for(int i=0;i<brojKlijenata;i++){
    			Klijent klijent = new Klijent(i);
    			klijent.start();
    			try {
					sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
    		}
  
    	}
    	
    }
    
    static public class Cekaonica {
    	private int zauzeto;
    	private int maxMjesta;
    	
    	public Cekaonica(){
    		zauzeto=0;
    		maxMjesta=rand.nextInt(3)+3;
    	}
    	
    	public boolean addKlijenta(){
    		if(zauzeto>=maxMjesta){
    			return false;
    		} else {
    			zauzeto++;
    			return true;
    		}
    	}
    	
    	public int brZauzetih(){
    		return zauzeto;
    	}
    	
    	public boolean removeKlijenta(){
    		if(zauzeto<=0){
    			return false;
    		} else {
    			zauzeto--;
    			return true;
    		}
    	}
   
    	public boolean isFull(){
    		if(zauzeto>=maxMjesta){
    			return true;
    		} else {
    			return false;
    		}
    	}
    	
    	public boolean isEmpty(){
    		if(zauzeto<=0){
    			return true;
    		} else {
    			return false;
    		}
    	}
    }
    
    static public class ZatvoriRadnju extends Thread{
    	
    	@Override
    	public void run(){
    		long currentTime=System.currentTimeMillis();
    		try {
				sleep(rand.nextInt(5000)+5000);
				semOtvoreno.acquire();
				otvoreno=false;
				System.out.println("Radno vrijeme gotovo nakon "+((double)(System.currentTimeMillis()-currentTime)/1000)+" seconds.");
				semSpavanje.release();
				semOtvoreno.release();
			} catch (InterruptedException e) {
			}
    	}
    }
	
	public static void main(String[] args) {
		Frizerka frizerka = new Frizerka();
		frizerka.start();
		KlijentGenerator kg = new KlijentGenerator();
		kg.start();
		new ZatvoriRadnju().start();
		
		
	}

}
