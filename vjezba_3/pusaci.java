
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class pusaci {
	static ArrayList<Sastojak> listaSastojaka = new ArrayList<>(Arrays.asList(new Sastojak("papir"), new Sastojak("duhan"), new Sastojak("sibice")));
	static ArrayList<Sastojak> postavljeni = new ArrayList<>();
	
	static Random rand= new Random();
	
	static boolean isPlaced = false;
	
	static Object key = new Object();
	
	
	static public class Sastojak{
		private String sastojak;
		
		public Sastojak(String sastojak) {
			this.sastojak=sastojak;
		}

		public String getSastojak() {
			return sastojak;
		}
	
		
    }
	
	static public class Trgovac extends Thread{
		
		@Override
		public void run(){
			while(true){
				synchronized (key) {
					
					while(isPlaced){
						try {
							key.wait();
						} catch (InterruptedException e) {
						}
					}
					Sastojak prvi = listaSastojaka.get(rand.nextInt(listaSastojaka.size()));
					Sastojak drugi;
					while(true){
						drugi = listaSastojaka.get(rand.nextInt(listaSastojaka.size()));
						if(!drugi.getSastojak().equals(prvi.getSastojak())){
							break;
						}
					}
					postavljeni.add(prvi);
					postavljeni.add(drugi);
					
					isPlaced=true;
					key.notifyAll();
					System.out.println("Trgovac stavlja: " + postavljeni.get(0).getSastojak() + " i "+ postavljeni.get(1).getSastojak());
				}
			}
		}
		
	}
	
	static public class Pusac extends Thread{
		private Sastojak imam;
		int id;
		
		public Pusac(Sastojak sastojak, int id){
			this.imam = sastojak;
			this.id=id;
		}
		
		@Override
		public void run(){
			System.out.println(this+": ima "+ imam.getSastojak());
			while(true){
				synchronized (key) {
					while(!isPlaced || postavljeni.contains(imam)){
						try {
							key.wait();
						} catch (InterruptedException e) {
						}
					}
					postavljeni.remove(0);
					postavljeni.remove(0);
					isPlaced=false;
					key.notifyAll();
					System.out.println(this+": uzima sastojke i ...");				
				}
				pusenje();
			}
		}
		
		public void pusenje(){
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		@Override
		public String toString(){
			return "Pusac "+id;
		}
	}
	
	
	public static void main(String[] args) {
		for(int i=0;i<listaSastojaka.size();i++){
			new Pusac(listaSastojaka.get(i),i+1).start();
		}
		new Trgovac().start();
		

	}

}
