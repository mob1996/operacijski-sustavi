import java.util.ArrayList;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;

public class PlacementSimulation extends Thread {
	
	private LinkedBlockingQueue<ThreadStruct> onArrival;
	private ArrayList<ThreadStruct> threadQueue;
	private int time;
	
	
	public PlacementSimulation(LinkedBlockingQueue<ThreadStruct> onArrival, ArrayList<ThreadStruct> threadQueue) {
		time=0;
		this.onArrival = onArrival;
		this.threadQueue = threadQueue;
	}
	
	
	@Override
	public void run(){
		System.out.printf("  t     AKT     PR1     PR2      PR3     PR4     PR5\n");
		while(true){
			
			try {
				visekriterijsko.openSem.acquire();
				visekriterijsko.onArrivalSem.acquire();
				if(visekriterijsko.isOpen==false && onArrival.isEmpty() && threadQueue.isEmpty()){
					visekriterijsko.openSem.release();
					visekriterijsko.onArrivalSem.release();
					return;
				}
				visekriterijsko.openSem.release();
				visekriterijsko.onArrivalSem.release();
			} catch (InterruptedException e) {
			}	
			
			
			printState();
			try {
				sleep(500);
			} catch (InterruptedException e) {
			}
			time++;
		}
		
	}
	
	
	private void printState(){
		if(threadQueue.isEmpty()){
			print();
			if(checkOnArrival()){
				print();
			}
		} else {
			ThreadStruct active = threadQueue.get(0);
			active.decrementTime();
			if(active.getWp()==WayOfPlacement.RR){
				if(active.getRemainingTime()<=0){
					threadQueue.remove(0);
					print();
					System.out.println("Dretva " + active.getId() +" je zavrsila sa radom.");
					if(checkOnArrival()){
						print();
					}
				} else {
					print();
					checkOnArrival();
					threadQueue.add(threadQueue.remove(0));
					threadQueue.sort(null);
					print();
				}
			} else if(active.getWp()==WayOfPlacement.FIFO){
				if(active.getRemainingTime()<=0){
					threadQueue.remove(0);
					print();
					System.out.println("Dretva " + active.getId() +" je zavrsila sa radom.");
					if(checkOnArrival()){
						print();
					}
				} else {
					print();
					if(checkOnArrival()){
						print();
					}
				}
			}
		}
	}
	
	private void print(){
			int capacity = 6;
			System.out.printf("%3d", time);
			for(ThreadStruct ts : threadQueue){
				capacity--;
				System.out.printf("    %d/%d/%d", ts.getId(), ts.getPriority(), ts.getRemainingTime());
			}
			for(;capacity>0;capacity--){
				System.out.print("   -/-/-");
			}
			System.out.println();
	}
	
	private boolean checkOnArrival(){
		boolean ret = false;
		try {
			visekriterijsko.onArrivalSem.acquire();
			for(ThreadStruct ts : onArrival){
				if(threadQueue.size()<6){
					System.out.printf("%3d -- nova dretva id=%2d, runningTime = %2d, prio= %2d, type = %s\n", time, ts.getId(), ts.getRemainingTime(), ts.getPriority(), ts.getType());
					threadQueue.add(ts);
					ret=true;
				} else {
					System.out.printf("%3d -- nova dretva id=%2d, runningTime = %2d, prio= %2d, type = %s je odbijena zbog nedostatka memorijskog prostora\n", time, ts.getId(), ts.getRemainingTime(), ts.getPriority(), ts.getType());
				}
			}
			onArrival.clear();
			threadQueue.sort(null);
			visekriterijsko.onArrivalSem.release();
		} catch (InterruptedException e) {
		}
		
		return ret;
	}
	
	
	
}
