import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

public class ThreadGenerator extends Thread {

	private Random rand = new Random();
	private LinkedBlockingQueue<ThreadStruct> onArrival;
	
	public ThreadGenerator(LinkedBlockingQueue<ThreadStruct> onArrival){
		this.onArrival = onArrival;
	}
	
	public void run(){
		int numOfGeneratedThreads = 0;
		while(numOfGeneratedThreads<=10){
			int id=numOfGeneratedThreads;
			int priority = rand.nextInt(10)+1; //1-10
			int remainingTime = rand.nextInt(5)+1; //1-5
			int placementType = rand.nextInt(2);
			WayOfPlacement wp;
			if(placementType==0){
				wp=WayOfPlacement.RR;
			} else {
				wp=WayOfPlacement.FIFO;
			}
			
			ThreadStruct ts = new ThreadStruct(id, remainingTime, 6, wp);
			onArrival.add(ts);
			numOfGeneratedThreads++;
			
			try {
				sleep(rand.nextInt(500)+500);
			} catch (InterruptedException e) {
			}
		}
		try {
			visekriterijsko.openSem.acquire();
			visekriterijsko.isOpen = false;
			visekriterijsko.openSem.release();
		} catch (InterruptedException e) {
		}
		
	}
}
