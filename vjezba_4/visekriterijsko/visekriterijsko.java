import java.util.ArrayList;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

public class visekriterijsko {

	public static Semaphore openSem = new Semaphore(1);
	public static Semaphore onArrivalSem = new Semaphore(1);
	public static boolean isOpen = true;
	
	public static void main(String[] args) {
		LinkedBlockingQueue<ThreadStruct> onArrival = new LinkedBlockingQueue<>();
		ArrayList<ThreadStruct> threadQueue = new ArrayList<>();

		PlacementSimulation ps = new PlacementSimulation(onArrival, threadQueue);
		ThreadGenerator tg = new ThreadGenerator(onArrival);
		
		ps.start();
		tg.start();
		
		try {
			ps.join();
			tg.join();
		} catch (InterruptedException e) {
		}
		
	}

}
