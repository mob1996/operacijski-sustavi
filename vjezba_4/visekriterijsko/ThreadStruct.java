

public class ThreadStruct implements Comparable<ThreadStruct> {
	private int id;
	private int remainingTime;
	private int priority;
	private WayOfPlacement wp;
	private boolean arrivalNoticed = false;
	private String type;
	
	public ThreadStruct(int id, int remainingTime, int priority, WayOfPlacement wp) {
		this.id = id;
		this.remainingTime = remainingTime;
		this.priority = priority;
		this.wp = wp;
		if(this.wp==WayOfPlacement.FIFO){
			type="FIFO";
		} else if(this.wp==WayOfPlacement.PRIO){
			type="PRIO";
		} else {
			type="RR";
		}
	}
	
	public boolean isOver(){
		if(remainingTime<=0){
			return true;
		} else {
			return false;
		}
	}
	
	public void decrementTime(){
		remainingTime--;
	}

	public boolean isArrivalNoticed() {
		return arrivalNoticed;
	}

	public void ArrivalNoticed() {
		this.arrivalNoticed = true;
	}

	public int getId() {
		return id;
	}

	public int getRemainingTime() {
		return remainingTime;
	}

	public int getPriority() {
		return priority;
	}

	public WayOfPlacement getWp() {
		return wp;
	}
	
	public String getType() {
		return type;
	}

	@Override
	public String toString(){
		return "Dretva -"+" id = " + id+" - Priority = "+ priority+ " - Remaining time = "+remainingTime;
	}

	@Override
	public int compareTo(ThreadStruct o) {
		int r =  this.priority-o.getPriority();
		if(r>0){
			return -1;
		} if(r<0){
			return 1;
		}
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ThreadStruct other = (ThreadStruct) obj;
		if (id != other.id)
			return false;
		return true;
	}


	
	
	

}
