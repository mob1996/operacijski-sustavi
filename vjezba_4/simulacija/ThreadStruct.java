
public class ThreadStruct {
	private int id;
	private int remainingTime;
	private int priority;
	private WayOfPlacement wp;
	private boolean arrivalNoticed = false;
	
	
	public ThreadStruct(int id, int remainingTime, int priority, WayOfPlacement wp) {
		this.id = id;
		this.remainingTime = remainingTime;
		this.priority = priority;
		this.wp = wp;
		
	}
	
	public boolean isOver(){
		if(remainingTime<=0){
			return true;
		} else {
			return false;
		}
	}
	
	public void decrementTime(){
		remainingTime--;
	}

	public boolean isArrivalNoticed() {
		return arrivalNoticed;
	}

	public void ArrivalNoticed() {
		this.arrivalNoticed = true;
	}

	public int getId() {
		return id;
	}

	public int getRemainingTime() {
		return remainingTime;
	}

	public int getPriority() {
		return priority;
	}

	public WayOfPlacement getWp() {
		return wp;
	}
	
	@Override
	public String toString(){
		return "Dretva -"+" id = " + id+" - Priority = "+ priority+ " - Remaining time = "+remainingTime;
	}
	

}
