
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

public class simulacija {

	static public Semaphore openSem = new Semaphore(1);
	static public Semaphore onArrivalSem = new Semaphore(1);
    static public boolean isOpen = true;
	
	public static void main(String[] args) {
		LinkedBlockingQueue<ThreadStruct> onArrival = new LinkedBlockingQueue<>();
		ArrayBlockingQueue<ThreadStruct> threadQueue = new ArrayBlockingQueue<>(6);
		ThreadGenerator tg = new ThreadGenerator(onArrival);
		RoundRobinSim rrs = new RoundRobinSim(onArrival, threadQueue);
		
		tg.start();
		rrs.start();
		
		try {
			tg.join();
			rrs.join();
		} catch (InterruptedException e) {

		}

	}

}
