import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

public class ThreadGenerator extends Thread {

	private Random rand = new Random();
	private LinkedBlockingQueue<ThreadStruct> onArrival;
	
	public ThreadGenerator(LinkedBlockingQueue<ThreadStruct> onArrival){
		this.onArrival = onArrival;
	}
	
	public void run(){
		int numOfGeneratedThreads = 0;
		while(numOfGeneratedThreads<=10){
			int id=numOfGeneratedThreads;
			int priority = rand.nextInt(10)+1; //1-10
			int remainingTime = rand.nextInt(5)+1; //1-5
			WayOfPlacement wp = WayOfPlacement.RR;
			
			ThreadStruct ts = new ThreadStruct(id, remainingTime, priority, wp);
			try {
				simulacija.onArrivalSem.acquire();
				onArrival.add(ts);
				simulacija.onArrivalSem.release();
			} catch (InterruptedException e1) {
			}
			
			numOfGeneratedThreads++;
			
			try {
				sleep(rand.nextInt(500)+500);
			} catch (InterruptedException e) {
			}
		}
		try {
			simulacija.openSem.acquire();
			simulacija.isOpen = false;
			simulacija.openSem.release();
		} catch (InterruptedException e) {
		}
		
	}
}
