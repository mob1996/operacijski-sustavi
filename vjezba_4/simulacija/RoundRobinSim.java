import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class RoundRobinSim extends Thread {
	
	private LinkedBlockingQueue<ThreadStruct> onArrival;
	private ArrayBlockingQueue<ThreadStruct> threadQueue;
	private int time;
	
	public RoundRobinSim(LinkedBlockingQueue<ThreadStruct> onArrival,
			ArrayBlockingQueue<ThreadStruct> threadQueue) {
		this.onArrival = onArrival;
		this.threadQueue = threadQueue;
		this.time = 0;
	}
	
	public void run(){
		System.out.printf("  t     AKT     PR1     PR2      PR3     PR4     PR5\n");
		while(true){
			
			try {
				simulacija.openSem.acquire();
				simulacija.onArrivalSem.acquire();
				if(simulacija.isOpen==false && threadQueue.isEmpty() && onArrival.isEmpty()){
					simulacija.openSem.release();
					simulacija.onArrivalSem.release();
					return;
				}
				simulacija.openSem.release();
				simulacija.onArrivalSem.release();
			} catch (InterruptedException e) {
			}
			
			printState();
			time++;
			try {
				sleep(500);
			} catch (InterruptedException e) {
			}
			
		}
	}
	
	
	private void printState(){
		if(!threadQueue.isEmpty()){
			threadQueue.peek().decrementTime();
			if(threadQueue.peek().getRemainingTime()<=0){
				ThreadStruct end = threadQueue.remove();
				print();
				System.out.println("Dretva " + end.getId() +" je zavrsila sa radom.");
				checkOnArrival();
				print();
			} else {
				print();
				checkOnArrival();
				threadQueue.add(threadQueue.poll());
				print();
			}
		} else {
			print();
			if(checkOnArrival()){
				print();
			}
		}

	}
	
	private void print(){
		int capacity = 6;
		System.out.printf("%3d", time);
		for(ThreadStruct ts : threadQueue){
			capacity--;
			System.out.printf("    %d/%d/%d", ts.getId(), ts.getPriority(), ts.getRemainingTime());
		}
		for(;capacity>0;capacity--){
			System.out.print("   -/-/-");
		}
		System.out.println();
	}
	
	private boolean checkOnArrival(){
		boolean ret = false;
		try {
			simulacija.onArrivalSem.acquire();
			for(ThreadStruct ts : onArrival){
				if(threadQueue.remainingCapacity()>0){
					System.out.printf("%3d -- nova dretva id=%2d, runningTime = %2d, prio= %2d\n", time, ts.getId(), ts.getRemainingTime(), ts.getPriority());
					threadQueue.add(ts);
					ret=true;
				} else {
					System.out.printf("%3d -- nova dretva id=%2d, runningTime = %2d, prio= %2d je odbijena zbog nedostatka memorijskog prostora\n", time, ts.getId(), ts.getRemainingTime(), ts.getPriority());
				}
			}
			onArrival.clear();
			simulacija.onArrivalSem.release();
		} catch (InterruptedException e) {
		}
		
		return ret;
	}
	
	
	
}
